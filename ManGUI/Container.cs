using System;
using Microsoft.Extensions.DependencyInjection;

namespace ManGUI
{
    /// <summary>
    /// Static container to make ServiceProvider and program arguments accessible.
    /// </summary>
    public static class Container
    {
        private static IServiceProvider _serviceProvider = null!;
        public static IServiceProvider ServiceProvider
        {
            get => _serviceProvider;
            set => _serviceProvider ??= value;
        }

        private static string[] _arguments = null!;
        public static string[] Arguments
        {
            get => _arguments;
            set => _arguments ??= value;
        }
    }
}