using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using ManGUI.ViewModels;
using ManGUI.Views;

namespace ManGUI
{
    public class App : Application
    {
        public override void Initialize()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public override void OnFrameworkInitializationCompleted()
        {
            if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                var windowViewModel = new MainWindowViewModel();
                var window = new MainWindow
                {
                    DataContext = windowViewModel
                };
                desktop.MainWindow = window;
                windowViewModel.Window = window;
            }

            base.OnFrameworkInitializationCompleted();
        }
    }
}