using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace ManGUI.Views
{
    public class DetailsWindow : Window
    {
        public DetailsWindow()
        {
            InitializeComponent();
#if DEBUG
            this.AttachDevTools();
#endif
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}