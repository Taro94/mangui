using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DynamicData;

namespace ManGUI.Helpers
{
    public interface IMango
    {
        bool InstallInProgress { get; }
        bool GetIsMangoInstalled();
        string GetVersion();
        bool GetIsPackageInstalled(string archPackage);
        List<string> GetArchPackagesInMangoPackage(string file);
        int GetNumberOfInstalledPackagesFromList(List<string> packages);
        string GetPackageDetails(string file);
        bool GetIsRoot();
        string GetMangoPackageName(string file);
        public bool GetDoConflictsExist(string file);

        /// <summary>
        /// Start the installation of a mango package.
        /// </summary>
        /// <param name="file">Path to the mango package file to install</param>
        /// <param name="noRoot">Returns true if the user is not root and no supported sudo frontend is installed</param>
        /// <returns>true if installation has started, false if it hasn't, because another installation is in progress</returns>
        bool StartInstallation(string file, out bool noRoot);
    }

    public class Mango : IMango
    {
        public bool InstallInProgress { get; private set; } = false;

        public bool GetIsMangoInstalled()
        {
            var output = "command -v mango".Bash();
            return !string.IsNullOrEmpty(output);
        }

        public string GetVersion()
        {
            var output = "mango --version".Bash(true);
            var version = output.Split(' ')[1].TrimEnd('\n');
            return version;
        }

        public bool GetIsPackageInstalled(string archPackage)
        {
            var output = $"pacman -Q | grep \"{archPackage} \"".Bash(true);
            var installed = !string.IsNullOrEmpty(output);
            return installed;
        }

        public List<string> GetArchPackagesInMangoPackage(string file)
        {
            var output = $"mango package \"{file}\"".Bash(true);
            var outputArray = output.Split('\n');
            int endIndex = outputArray.IndexOf("Missing optional dependencies:");
            var list = outputArray
                .Where(x => x.StartsWith("- ") && outputArray.IndexOf(x) < endIndex)
                .Select(x => x[2..])
                .ToList();
            return list;
        }

        public bool GetDoConflictsExist(string file)
        {
            return $"mango package \"{file}\"".Bash()
                .Trim('\n').Split('\n')
                .Any(x => x == "Conflicting installed Arch packages:");
        }

        public string GetMangoPackageName(string file)
        {
            var output = $"mango package \"{file}\" | grep \"Package name: \"".Bash();
            var name = output.Substring(14).TrimEnd('\n');
            return name;
        }

        public int GetNumberOfInstalledPackagesFromList(List<string> packages)
        {
            var builder = new StringBuilder("pacman -Q | grep ");
            foreach (var package in packages)
            {
                builder.Append($"-e \"{package} \" ");
            }

            var command = builder.ToString();
            var output = command.Bash(true);
            var result = output.Split(Environment.NewLine).Where(x => !string.IsNullOrEmpty(x)).ToList().Count;

            return result;
        }

        public bool GetIsRoot()
        {
            var output = "echo $EUID".Bash().Trim('\n');
            return output == "0";
        }

        private string GetInstalledGUIAuthorizationTool()
        {
            var tools = new List<string>
            {
                "pkexec",
                "gksu",
                "lxqt-sudo",
            };

            foreach (var tool in tools)
            {
                if (!string.IsNullOrEmpty($"command -v {tool}".Bash()))
                    return tool;
            }

            return null!;
        }

        public string GetPackageDetails(string file)
        {
            var output = $"mango package \"{file}\"".Bash();
            return output;
        }

        /// <summary>
        /// Start the installation of a mango package.
        /// </summary>
        /// <param name="file">Path to the mango package file to install</param>
        /// <param name="noRoot">Returns true if the user is not root and no supported sudo frontend is installed</param>
        /// <returns>true if installation has started, false if it hasn't, because another installation is in progress</returns>
        public bool StartInstallation(string file, out bool noRoot)
        {
            noRoot = false;

            if (InstallInProgress)
                return false;

            var tool = GetInstalledGUIAuthorizationTool();
            if (tool == null)
            {
                noRoot = true;
                return false;
            }

            InstallInProgress = true;
            Task.Run(() =>
            {
                var result = $"{tool} mango install \"{file}\"".Bash(true);
                InstallInProgress = false;
                return result;
            });

            return true;
        }
    }
}