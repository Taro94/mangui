using System;
using System.Diagnostics;
using System.IO;
using System.Reflection.Metadata;
using Microsoft.CodeAnalysis;

namespace ManGUI.Helpers
{
    public static class ShellHelper
    {
        public static string ExecuteBashScript(string scriptFilename, params string[] arguments)
        {
            var content = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, scriptFilename));
            var argNum = 1;
            foreach (var arg in arguments)
            {
                content = content.Replace($"${argNum}", $"\"{arg}\"");
                argNum++;
            }
            
            return content.Bash();
        }
        
        public static string ExecuteBashScriptAndIgnoreErrors(string scriptFilename, params string[] arguments)
        {
            var content = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, scriptFilename));
            var argNum = 1;
            foreach (var arg in arguments)
            {
                content = content.Replace($"${argNum}", $"\"{arg}\"");
                argNum++;
            }
            
            return content.Bash(true);
        }

        public static string Bash(this string cmd, bool redirectError=false)
        {
            var escapedArgs = cmd.Replace("\"", "\\\"");

            var process = new Process()
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "/bin/bash",
                    Arguments = $"-c \"{escapedArgs}\"",
                    RedirectStandardOutput = true,
                    RedirectStandardError = redirectError,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                }
            };

            process.Start();
            string result = process.StandardOutput.ReadToEnd();
            string error = !redirectError ? string.Empty : process.StandardError.ReadToEnd();
            process.WaitForExit();
            if (redirectError && !string.IsNullOrEmpty(error))
                return error;
            return result;
        }
    }
}