﻿using System;
using System.Diagnostics;
using System.IO;
using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.ReactiveUI;
using ManGUI.Helpers;
using Microsoft.CodeAnalysis.Diagnostics;
using Microsoft.Extensions.DependencyInjection;

namespace ManGUI
{
    class Program
    {
        // Initialization code. Don't use any Avalonia, third-party APIs or any
        // SynchronizationContext-reliant code before AppMain is called: things aren't initialized
        // yet and stuff might break.
        [STAThread]
        public static void Main(string[] args)
        {
            //Set up DI
            var serviceProvider = new ServiceCollection()
                .AddSingleton<IMango, Mango>()
                .BuildServiceProvider();
            Container.ServiceProvider = serviceProvider;
            Container.Arguments = args;
            
            //Return on any errors
            if (args.Length < 1)
            {
                Console.Error.WriteLine("Provide a path to a mango package file, ex: \"mangui coolsoftware.mango\"");
                return;
            }

            string path = string.Empty;
            foreach (var arg in args)
                path += arg + " ";
            path = path.Trim();
            args[0] = path;
            if (!File.Exists(path))
            {
                Console.Error.WriteLine("File does not exist.");
                return;
            }

            if (!serviceProvider.GetService<IMango>()!.GetIsMangoInstalled())
            {
                Console.Error.WriteLine("mango is not installed.");
                return;
            }
            
            //Build avalonia app
            BuildAvaloniaApp()
                .StartWithClassicDesktopLifetime(args);
        }

        // Avalonia configuration, don't remove; also used by visual designer.
        public static AppBuilder BuildAvaloniaApp()
            => AppBuilder.Configure<App>()
                .UsePlatformDetect()
                .LogToTrace()
                .UseReactiveUI();
    }
}