﻿using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;
using ReactiveUI;

namespace ManGUI.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        private ViewModelBase _content = null!;
        public ViewModelBase Content
        {
            get => _content;
            private set => this.RaiseAndSetIfChanged(ref _content, value);
        }

        private Window _window = null!;

        public Window Window
        {
            get => _window;
            set => _window ??= value;
        }

        public string WindowTitle { get; set; } = null!;

        public MainWindowViewModel()
        {
            Content = new InstallViewModel(this);
        }
    }
}