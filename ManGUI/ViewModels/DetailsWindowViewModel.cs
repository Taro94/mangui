namespace ManGUI.ViewModels
{
    public class DetailsWindowViewModel : ViewModelBase
    {
        public string Details { get; private set; }
        
        public DetailsWindowViewModel(string details)
        {
            Details = details;
        }
    }
}