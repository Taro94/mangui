﻿using System.Threading.Tasks;
using ManGUI.Helpers;
using ManGUI.Views;
using Microsoft.Extensions.DependencyInjection;
using ReactiveUI;

namespace ManGUI.ViewModels
{
    public class InstallViewModel : ViewModelBase
    {
        private readonly IMango _mango;
        private MainWindowViewModel _mainWindowViewModel;
        private string _packageName;

        private int _progressValue = 0;
        public int ProgressValue
        {
            get => _progressValue;
            set => this.RaiseAndSetIfChanged(ref _progressValue, value);
        }

        private bool _installStarted = false;

        public bool InstallStarted
        {
            get => _installStarted;
            set => this.RaiseAndSetIfChanged(ref _installStarted, value);
        }

        private string _topMessage = null!;
        public string TopMessage
        {
            get => _topMessage;
            set => this.RaiseAndSetIfChanged(ref _topMessage, value);
        }
        
        private string _bottomMessage = null!;
        public string BottomMessage
        {
            get => _bottomMessage;
            set => this.RaiseAndSetIfChanged(ref _bottomMessage, value);
        }

        public InstallViewModel(MainWindowViewModel mainWindowViewModel)
        {
            _mainWindowViewModel = mainWindowViewModel;
            _mango = Container.ServiceProvider!.GetService<IMango>()!;
            _packageName = _mango.GetMangoPackageName(Container.Arguments[0]);
            _mainWindowViewModel.WindowTitle = $"{_packageName} - manGUI";

            TopMessage = $"{_packageName} installation";
            BottomMessage = $"You are about to install software from a mango package.";
        }

        private void SetProgress(int installed, int total)
        {
            var fraction = (double)installed / total;
            var progress = (int)(fraction * 100);
            ProgressValue = progress;
        }

        public void Exit()
        {
            _mainWindowViewModel.Window.Close();
        }

        public void ShowDetails()
        {
            var details = _mango.GetPackageDetails(Container.Arguments[0]);
            var sudoPromptWindow = new DetailsWindow
            {
                DataContext = new DetailsWindowViewModel(details)
            };
            sudoPromptWindow.ShowDialog(_mainWindowViewModel.Window);
        }
        
        public void StartInstallation()
        {
            var file = Container.Arguments[0];
            var list = _mango.GetArchPackagesInMangoPackage(file);
            var packagesToBeInstalled = list.Count;

            if (_mango.StartInstallation(file, out bool noRoot) == false)
            {
                if (noRoot)
                    BottomMessage = $"You are not authorized to install software.";
                return;
            }

            if (_mango.GetDoConflictsExist(file))
            {
                BottomMessage =
                    "Installation can't proceed because of conflicts with already installed Arch packages. See package details.";
                return;
            }

            BottomMessage = $"Installing {_packageName}...";
            InstallStarted = true;

            Task.Run(async () =>
            {
                while (_mango.InstallInProgress)
                {
                    var installed = _mango.GetNumberOfInstalledPackagesFromList(list);
                    SetProgress(installed, packagesToBeInstalled);
                    await Task.Delay(1000);
                }
                var installedFinal = _mango.GetNumberOfInstalledPackagesFromList(list);
                if (installedFinal == packagesToBeInstalled)
                {
                    BottomMessage = $"Package {_packageName} installed successfully!";
                    ProgressValue = 100;
                }
                else
                {
                    BottomMessage = $"Package {_packageName} could not be installed.";
                    ProgressValue = 0;
                    InstallStarted = false;
                }
            });
        }
    }
}